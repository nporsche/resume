## Personal information
* Name: Hang Chen
* Age: 37
* Birthday: 1985.10.21
* Address: Changping District, Beijing
* Email: nporsche@163.com
* Tel: 13522759570
****
## Education background
* Huazhong University of Science and Technology (2004.9-2008.6) Bachelor of Software Engineering
* Tsinghua University (2012.9-2014.8) Master of Engineering Management
****
## Expertise
* Linux backend high-performance service development, design, optimization, architecture
* kubernetes, containerized cluster management
* Global Traffic Management
* Business architecture based on AWS services
* Design, optimization and architecture of Service Mesh with 30w+ instance scale
* Rich experience in large-scale and cross-team (product, operation, operation and maintenance, system) project management
* Rich experience for remote(multiple time zones) team/project management of up and down.
* Good at predicting and abstracting business development, and designing and implementing infrastructure and service architecture in advance
****
## Work experience
![Timeline](https://gitlab.com/nporsche/resume/-/raw/master/experience_en.png)

### Shopee (2021.8-present)
* Team: SPEX(Shopee Platform EXchange)
* Role: dataplane team leader
* Position: Senior Expert Engineer
* Number of subordinates: 7
* SPEX Introduction: Service mesh platform that full-stack developed by Shopee, including control-plane and dataplane. Currently support 30w+ service instances, 3w+ nodes accross multiple datacenters globally. It provides:
	* service registery and discovery
	* command-based RPC
	* config center
	* traffic management
	* ratelimit, circuit breaker, outlier detection, chaos-injection, traffic recording
	* observability 
* Job Responsibilities:
	* Responsible for SPEX dataplane team's regular design, development, code review and people management
	* Participate as SPEX PIC and support company-level projects, like split by market, full-chain stress test, PFB(per feature branch)
	* Hands on some key projects with overall design, coding and project management. E.g. zero-loss graceful upgrade, global traffic mangement, x-prootocl support.

### Freewheel (2017.7-2021.7)
* Team: Advertising Decision Engine Infrastructure Group
* Position: Principle Engineer
* Number of subordinates: 29
* Job Responsibilities:
	* Design and implement global containerized adserver and dependencies, use kubernetes service to go online, grayscale, and rollback
	* Design and implement the global adserver playground environment, providing customers with a completely real and safe online stress testing environment
	* Design and implement Global Traffic Scheduling (GTM), hybrid cloud architecture (AWS+Onprem) to improve peak computing capacity while saving costs in normal times
	* Design the next-generation golang version of the adserver high-performance business framework (GADS), rewrite the ad http server, thread scheduling, asynchronous IO, and implement it smoothly
	* Solve various stuck technical problems in the team, memory leaks, golang GC BUG, etc.
	* Member of the company's CCoE (Cloud Center of Excellence) committee, formulating company-level cloud architecture standards and helping business teams implement them
	* Publication of personal technical articles
		* [Supports live ads for super sports events, why is FreeWheel so powerful? ](https://zhuanlan.zhihu.com/p/263652622)

### Impulse travel (2017.2-2017.7) 9 months
* Team: Sage
* Position: CTO
* Number of subordinates: 21
* Job Responsibilities:
	* Manage technology and product lines, establish all IT systems, and standardize processes
	* Design and plan products according to business model
	* Design and structure services according to products, and standardize operation and maintenance, testing, and project management systems
	* Write background core code, and guide team members in technology and business

### Didi Chuxing (2014.8-2016.8) 2 years
* Team: Infrastructure Department
* Position: Senior System Architecture Engineer
* Job Responsibilities:
	* Langley: project leader of big data service based on fastbit bitmap index
	* Message Center: background design and development of Didi client message center, and the person in charge of the entire message center
	* Talkteller: Head of Beanstalkd cluster middleware development
	* Achievement honor:
		* [fastbit algorithm library open source contribution](https://codeforge.lbl.gov/frs/shownotes.php?release_id=372)
		* [kubernetes HPA community speech](http://huiyi.csdn.net/activity/closed?project_id=2833)

* Hightlight:
	* 2 years of performance are all A
	* Excellent employee in 2015

### Schlumberger (Beijing) Technology Co., Ltd. (2010.3-2014.5) 4 years and 2 months
* Team: Maxwell Framework
* Position: Senior Technical Expert
* Job Responsibilities:
	* Core module leader of Maxwell acquisition project team
	* Lead the team to develop and launch the evolution of the Isolation of Acquisition architecture, which greatly improved the usability of Maxwell products and the competitiveness of similar products
* Hightlight:
	* The performance in the first year is Exceed Expectation (30%), and the annual performance from the second year is Outstanding (5%)

### Beijing Rising International Technology Co., Ltd. (2009.3-2010.3) 1 year
* Team: Rising Enterprise Edition
* Position: Software Engineer
* Job Responsibilities:
	* News subscription center developer 

	   
##	personal interest
* Strong enthusiasm for technology, has always maintained a keen sense of smell for various new technologies
* Self-discipline, love all kinds of sports and competitive sports(tennis)
* I like to think about the meaning of life, and I can draw different conclusions at different stages