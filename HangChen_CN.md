## 杭晨
* 求职意向：互联网后台架构师，技术负责人  
* 目前状态：在职
****
### 个人信息
* 年龄: 36  
* 生日: 1985.10.21  
* 地址: 北京市昌平区  
* 邮箱: nporsche@163.com  
* 电话: 13522759570  
****
### 教育背景
* 华中科技大学（2004.9-2008.6) 软件工程  本科
* 清华大学(2012.9-2014.8) 工程管理 硕士
****
### 擅长领域
* Linux服务端高性能服务开发，设计，优化，架构
* kubernetes，容器化集群化管理
* 全球负载均衡GTM
* 基于AWS服务的业务架构
* 30w+实例规模的Service Mesh的设计，优化，架构
* 丰富的大型公司级项目经验，跨团队(产品，运营，运维，系统)沟通协调能力
* 善于对业务的发展进行预判和抽象，并提前设计和落地基础设施和服务架构
****
### 工作经历
![时间轴](https://gitlab.com/nporsche/resume/-/raw/master/experience_cn.png)  

Shopee(2021.8-至今)
* 团队: 服务治理数据面
* 职位: Senior Expert Engineer
* 下属人数: 7
* 工作职责:
	* 负责Shopee当前SPEX服务网格数据面的设计、研发，管理工作
	* 服务治理接口人：对接公司级项目split by market, 项目大促保障，全链路压测
	* 一线负责某些核心项目，例如无损升级，全球流量管理。工作包括整体设计，项目管理，核心代码编写 

飞维美地(Freewheel)（2017.7-2021.7)
* 团队: 广告决策引擎基础架构组
* 职位: 首席工程师
* 下属人数: 29
* 工作职责:
	* 设计并落地全球容器化adserver以及依赖，利用kubernetes服务上线，灰度，回滚
	* 设计并落地全球adserver playground环境，提供给客户完全真实的并且安全的线上压测环境
	* 设计并落地全球流量调度(GTM)，混合云架构(AWS+Onprem)提高峰值计算能力同时在平时节省成本
	* 设计下一代golang版本的adserver高性能业务框架(GADS)，重写ad http server，线程调度，异步IO，并平稳落地
	* 解决团队中各种卡壳疑难技术问题，内存泄漏，golang GC BUG等
	* 公司CCoE(Cloud Center of Excellence)委员会成员，制定公司级别云架构的标准，并帮助业务团队落地
* 公开个人技术文章
	* [支持超级赛事直播广告，FreeWheel为何如此强大？](https://zhuanlan.zhihu.com/p/263652622)  

冲动旅行（2017.2-2017.7）9个月
* 团队: Sage 
* 职位: CTO
* 下属人数：21
* 工作职责:
	* 管理技术，产品两条线，建立所有的IT系统，规范流程
	* 根据商业模式来设计和规划产品
	* 根据产品来设计和架构服务，并且规范运维，测试，项目管理体系
	* 编写后台核心代码，并指导团队成员技术和业务

滴滴出行 (2014.8-2016.8) 2年  
* 团队: 基础架构部
* 职位: 高级系统架构工程师
* 工作职责: 
	* Langley:基于fastbit位图索引的大数据服务项目负责人  
	* 消息中心: 滴滴客户端消息中心后台设计开发，以及整个消息中心负责人  
	* Talkteller: Beanstalkd集群化中间件开发负责人  
* 成果荣誉：
	* [fastbit算法库开源contribution](https://codeforge.lbl.gov/frs/shownotes.php?release_id=372)  
	* [kubernetes HPA社区演讲](http://huiyi.csdn.net/activity/closed?project_id=2833)
	* 2年的performance都是A
	* 2015年优秀员工 

斯伦贝谢（北京）科技有限公司(2010.3-2014.5) 4年2个月
* 团队: Maxwell Framework  
* 职位: 高级技术专家  
* 工作职责:
	* Maxwell acquisition项目组核心模块负责人  
	* 主导并带领团队开发并上线Isolation of Acquisition架构演进，极大的提高了Maxwell产品的可用性和对同类产品的竞争性
* 成果荣誉：
	* 第一年的Performance为Exceed Expectation(30%)，从第二年起每年的Performance为Outstanding(5%) 

北京瑞星国际科技有限公司(2009.3-2010.3) 1年
* 团队: 瑞星企业版  
* 职位: 开发工程师
* 工作职责: 
	* 消息订阅中心开发人员
###	个人兴趣
* 强烈的技术热情，对各类新技术一直保持着敏锐的嗅觉
* 自律，热爱各类体育竞技运动
* 喜欢思考人生的意义，不同的阶段都能得出不同的结论
